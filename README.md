# Standings Sync

Alliance Auth app for cloning alliance standings and war targets to alts.

[![release](https://img.shields.io/pypi/v/aa-standingssync?label=release)](https://pypi.org/project/aa-standingssync/)
[![python](https://img.shields.io/pypi/pyversions/aa-standingssync)](https://pypi.org/project/aa-standingssync/)
[![django](https://img.shields.io/pypi/djversions/aa-standingssync?label=django)](https://pypi.org/project/aa-standingssync/)
[![pipeline](https://gitlab.com/ErikKalkoken/aa-standingssync/badges/master/pipeline.svg)](https://gitlab.com/ErikKalkoken/aa-standingssync/-/pipelines)
[![codecov](https://codecov.io/gl/ErikKalkoken/aa-standingssync/branch/master/graph/badge.svg?token=gHBi42fbSs)](https://codecov.io/gl/ErikKalkoken/aa-standingssync)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/ErikKalkoken/aa-standingssync/-/blob/master/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![chat](https://img.shields.io/discord/790364535294132234)](https://discord.gg/zmh52wnfvM)

## Content

- [Features](#features)
- [Screenshot](#screenshot)
- [How it works](#how-it-works)
- [Installation](#installation)
- [Updating](#updating)
- [Settings](#settings)
- [Permissions](#permissions)
- [Admin Functions](#admin-functions)
- [Feedback](#feedback)
- [Change Log](CHANGELOG.md)

## Features

The main purpose of this app is to enable non-alliance characters to have the same standings view of other pilots in game as their alliance main. This e.g. allows non-alliance scouts to correctly report blues and non-blues. Or JF pilots can see which other non-alliance characters on grid not blues and therefore a potential threat.

Here is an high level overview of the main features:

- Synchronize alliance contacts to chosen non-alliance characters
- Supports coalition usage with multiple alliances in the same Alliance Auth installation
- Synchronize alliance war targets as contacts with terrible standing
- Automatically deactivates synchronization when a user ceases to be eligible (e.g. main left the alliance)

## Screenshot

Here is a screenshot of the main screen.

![Main Screen](https://i.imgur.com/xGdoqsp.png)

## How it works

To enable non-alliance members to use alliance standings the personal contact of that character are replaced with the alliance contacts.

## Installation

### Step 1 - Check Preconditions

Please make sure you meet all preconditions before proceeding:

1. Standings Sync is a plugin for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth). If you don't have Alliance Auth running already, please install it first before proceeding. (see the official [AA installation guide](https://allianceauth.readthedocs.io/en/latest/installation/auth/allianceauth/) for details)

2. Standings Sync needs the app [django-eveuniverse](https://gitlab.com/ErikKalkoken/django-eveuniverse) to function. Please make sure it is installed, before continuing.

### Step 2 - Install app

Install into AA virtual environment with PIP install from PyPI:

```bash
pip install aa-standingssync
```

### Step 3 - Update Eve Online app

Update the Eve Online app used for authentication in your AA installation to include the following scopes:

```plain
esi-characters.read_contacts.v1
esi-characters.write_contacts.v1
esi-alliances.read_contacts.v1
```

### Step 4 - Configure AA settings

Configure your AA settings (`local.py`) as follows:

Add `'standingssync'` to `INSTALLED_APPS`

Add these lines add to bottom of your settings file:

```python
# settings for standingssync
CELERYBEAT_SCHEDULE['standingssync.run_regular_sync'] = {
    'task': 'standingssync.tasks.run_regular_sync',
    'schedule': crontab(minute=0, hour='*/2')
}
```

Please also see the [settings](#settings) section for more configuration options. For example a setting is required to enable syncing war targets.

### Step 5 - Finalize installation into AA

Run migrations & copy static files

```bash
python manage.py migrate
python manage.py collectstatic --noinput
```

Restart your supervisor services for AA

### Step 6 - Setup permissions

Now you can access Alliance Auth and setup permissions for your users. See section "Permissions" below for details.

### Step 7 - Setup alliance character

Finally you need to set the alliance character that will be used for fetching the alliance contacts / standing. Just click on "Set Alliance Character" and add the requested token. Note that only users with the appropriate permission will be able to see and use this function.

Once an alliance character is set the app will immediately start fetching alliance contacts. Wait a minute and then reload the page to see the result.

That's it. The Standing Sync app is fully installed and ready to be used.

## Updating

To update your existing installation of Alliance Freight first enable your virtual environment.

Then run the following commands from your AA project directory (the one that contains `manage.py`).

```bash
pip install -U aa-standingssync
```

```bash
python manage.py migrate
```

```bash
python manage.py collectstatic --noinput
```

Finally restart your AA supervisor services.

## Settings

Here is a list of available settings for this app. They can be configured by adding them to your AA settings file (`local.py`). If they are not set the defaults are used.

Name|Description|Default
--|--|--
`STANDINGSSYNC_ADD_WAR_TARGETS`|When enabled will automatically add or set war targets  with standing = -10 to synced characters.|`False`
`STANDINGSSYNC_CHAR_MIN_STANDING`|Minimum standing a character needs to have in order to get alliance contacts. Any char with a standing smaller than this value will be rejected. Set to `0.0` if you want to allow neutral alts to sync.|`0.1`
`STANDINGSSYNC_REPLACE_CONTACTS`|When enabled will replace contacts of synced characters with alliance contacts.|`True`
`STANDINGSSYNC_STORE_ESI_CONTACTS_ENABLED`|Wether to store contacts received from ESI to disk. This is for debugging.|`False`
`STANDINGSSYNC_SYNC_TIMEOUT`|Duration in minutes after which a delayed sync for managers and characters is reported as down. This value should be aligned with the frequency of the sync task.|`180`
`STANDINGSSYNC_WAR_TARGETS_LABEL_NAME`|Name of EVE contact label for war targets. Needs to be created by the user for each synced character. Required to ensure that war targets are deleted once they become invalid. Not case sensitive.|`WAR TARGETS`

## Permissions

This app only uses two permission. One for enabling this app for users and one for enabling users to add alliances for syncing.

Name | Purpose | Code
-- | -- | --
Can add synced character |Enabling the app for a user. This permission should be enabled for everyone who is allowed to use the app (e.g. Member state) |  `add_syncedcharacter`
Can add alliance manager |Enables adding alliances for syncing by setting the character for fetching alliance contacts. This should be limited to users with admins / leadership privileges. |  `add_syncmanager`

## Admin functions

Admins will find a "Standings Sync" section on the admin page. This section provides the following features:

- See a list of all setup alliances with their sync status

- See a list of all enabled characters with their current sync status

- Manually remove characters / alliances from sync

- Manually start the sync process for characters / alliances

## Feedback

If you encounter any bugs or would like to request a new feature please open an issue in this gitlab repo.

"""Alliance Auth app for cloning alliance standings and war targets to alts."""

# pylint: disable = invalid-name
default_app_config = "standingssync.apps.SyncAltContactsConfig"

__version__ = "1.10.0"
__title__ = "Standings Sync"
